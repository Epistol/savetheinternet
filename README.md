savetheinternet
===============

european campaign website to protect the free and neutral internet [SaveTheInternet.eu](http://savetheinternet.eu)

##how can you help? 
- we always need help with translations in other languages or reviewers of the work of other translators
- talk about the campaign, tell your friends
- call MEPs, write e-mails, send them a fax or an letter
- [put an banner on your website](http://www.savetheinternet.eu/banner.zip "download banner pack")
- share the website on social media platforms
- be creative and come up with ideas we haven't thought about

###step by step explanation on how to propose changes on this website
- you make yourself a [github.com](https://github.com) account 
- go to [https://github.com/Netzfreiheit/savetheinternet/](https://github.com/Netzfreiheit/savetheinternet/)
- click “fork” 
- click “index.html” 
- click “edit” 
- make all your changes on the file
- click “commit changes” 
- click “pull requests” 
- click “Click to create a pull request for this comparison”
